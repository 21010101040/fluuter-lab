import 'dart:io';

void main(List<String> args) {
  count_odd_even();
}

List list() {
  print("Enter number of element in list");
  int n = int.parse(stdin.readLineSync()!);
  List list = new List.filled(n, 0);

  for (int i = 0; i < n; i++) {
    list[i] = int.parse(stdin.readLineSync()!);
  }
  print("List = $list");
  return (list);
}

void count_odd_even() {
  int odd = 0, even = 0;
  var read = list();
  for (int i = 0; i < read.length; i++) {
    if (read[i] % 2 == 0) {
      even++;
    }
    if (read[i] % 2 != 0) {
      odd++;
    }
  }

  print("Number of Even = $even");
  print("Number of Odd = $odd");
}
