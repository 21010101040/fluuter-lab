import 'dart:io';

void main(List<String> args) {
  print("Enter number");
  print(check(int.parse(stdin.readLineSync()!)));
}

String check(int n) {
  int a = 0;
  for (int i = 2; i < n; i++) {
    if (n % i == 0) {
      a = 1;
    }
  }

  if (a == 1) {
    return ("Number is not prime");
  } else {
    return ("Number is prime");
  }
}
