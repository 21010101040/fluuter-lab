import 'dart:io';

void main(List<String> args) {
  Area a = new Area();

  print("Enter Radius of Circle  ");

  print("Area of Circle is ${a.circle(double.parse(stdin.readLineSync()!))}");
  print("Enter Length and Height of Triangle  ");
  print(
      "Area of Triangle is ${a.triangle(double.parse(stdin.readLineSync()!), double.parse(stdin.readLineSync()!))}");
  print("Enter Lentgh of Square  ");
  print("Area of Square is ${a.square(double.parse(stdin.readLineSync()!))}");
}

class Area {
  double circle(double r) {
    return (3.14 * r * r);
  }

  double triangle(double l, double h) {
    return (0.5 * l * h);
  }

  double square(double l) {
    return (l * l);
  }
}
