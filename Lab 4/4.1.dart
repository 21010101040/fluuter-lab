import 'dart:io';

void main(List<String> args) {
  print("Enter Principle , Rate and Time.");

  print("${simple_interest(p: 10000, r: 3, t: 3)}");
}

double simple_interest(
    {required double p, required double t, required double r}) {
  return (p * t * r) / 100;
}
