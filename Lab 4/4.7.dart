import '4.6.dart';

void main(List<String> args) {
  sum_div_3or5();
}

void sum_div_3or5() {
  int sum = 0;
  var read = list();
  for (int i = 0; i < read.length; i++) {
    if (read[i] % 3 == 0 || read[i] % 5 == 0) {
      sum = read[i] + sum;
    }
  }

  print("Sum = $sum");
}
