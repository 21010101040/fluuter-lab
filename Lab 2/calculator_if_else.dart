import 'dart:io';

void main(List<String> args) {
  stdout.write("Enter n1 : ");
  int n1 = int.parse(stdin.readLineSync()!);

  stdout.write("Enter n2 : ");
  int n2 = int.parse(stdin.readLineSync()!);

  stdout.write("Enter Operator : ");
  String o = (stdin.readLineSync()!);

  if(o=="+"){
    print("Answer is : ${n1+n2}");
  }
  else if(o=="-"){
    print("Answer is : ${n1-n2}");
  }
  else if(o=="*"){
    print("Answer is : ${n1*n2}");
  }
  else if(o=="/"){
    print("Answer is : ${n1/n2}");
  }
  else{
    print("Invalid Operator");
  }

}