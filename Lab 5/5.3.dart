import 'dart:io';

void main(List<String> args) {
  print("Enter Radius of Circle = ");
  double? r = double.parse(stdin.readLineSync()!);
  Circle c = new Circle();

  print("Area of circle is ${c.area(r)}");
  print("Perimeter of circle is ${c.perimeter(r)}");
}

class Circle {
  area(r) {
    return 3.14 * r! * r!;
  }

  perimeter(r) {
    return 3.14 * 2 * r!;
  }
}
