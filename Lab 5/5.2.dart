import 'dart:io';

void main(List<String> args) {
  Bank_Account b = new Bank_Account();
  b.GetAccountDetails();
  b.DisplayAccountDetails();
}

class Bank_Account {
  int? Account_No;
  String? User_Name;
  String? Email;
  String? Account_Type;
  String? Account_Balance;

  GetAccountDetails() {
    print("Enter Account No = ");
    Account_No = int.parse(stdin.readLineSync()!);

    print("Enter User Name = ");
    User_Name = stdin.readLineSync();

    print("Enter Email = ");
    Email = stdin.readLineSync();

    print("Enter Account Type = ");
    Account_Type = stdin.readLineSync();

    print("Enter Account Balance = ");
    Account_Balance = stdin.readLineSync();
  }

  DisplayAccountDetails() {
    print("Account No = $Account_No");
    print("User Name = $User_Name");
    print("Email = $Email");
    print("Account Type = $Account_Type");
    print("Account Balance = $Account_Balance");
  }
}
