import 'dart:io';

void main(List<String> args) {
  print("Enter no=");

  int x = int.parse(stdin.readLineSync()!);
  int rem;

  while (x > 0) {
    rem = x % 10;
    x = x ~/ 10;

    stdout.write("$rem");
  }
}
