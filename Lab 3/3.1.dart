import 'dart:io';

void main(List<String> args) {
  print('Enter no 1.');
  int? no1 = int.parse(stdin.readLineSync()!);

  print('Enter no 2.');
  int? no2 = int.parse(stdin.readLineSync()!);

  for (int i = no1; i < no2; i++) {
    if (i % 2 == 0 && i % 3 != 0) {
      print(i);
    }
  }
}
