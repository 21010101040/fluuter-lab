import 'dart:io';

void main(List<String> args) {
  print('Enter no 1.');
  int? no = int.parse(stdin.readLineSync()!);

  int fac = 1;

  for (int i = 1; i <= no; i++) {
    fac = fac * i;
  }

  print("Factorial= $fac");
}
