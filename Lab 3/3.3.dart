import 'dart:io';

void main(List<String> args) {
  print("Enter no=");

  int x = int.parse(stdin.readLineSync()!);

  int? flag = 0;

  for (int i = 2; i < x; i++) {
    if (x % i == 0) {
      flag = 1;
    }
  }
  if (flag == 1) {
    print("Number is not prime");
  } else {
    print("Number is prime");
  }
}
