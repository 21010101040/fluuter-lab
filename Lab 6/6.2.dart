import 'dart:io';

void main(List<String> args) {
  print("How many element insert in the list?");
  int? n = int.parse(stdin.readLineSync()!);

  List<dynamic> a = new List.filled(n, 0, growable: false);

  for (int i = 0; i < n; i++) {
    stdout.write("Enter Element of First List.");
    a[i] = stdin.readLineSync();
  }

  List<dynamic> b = new List.filled(n, 0, growable: false);

  for (int i = 0; i < n; i++) {
    stdout.write("Enter Element of Second List.");
    b[i] = stdin.readLineSync();
  }

  print("List 1 = $a");
  print("List 2 = $b");

  for (int j = 0; j < n; j++) {
    for (int k = 0; k < n; k++) {
      if (a[j] == b[k]) {
        print("Same element = ${a[j]}");
      }
    }
  }
}
