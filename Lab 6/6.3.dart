import 'dart:io';

void main(List<String> args) {
  List str = ['Delhi', 'Mumbai', 'Bangalore', 'Hyderabad', 'Ahmedabad'];

  print("Old String = ${str}");

  for (int i = 0; i < str.length; i++) {
    if (str[i] == 'Ahmedabad') {
      str[i] = 'Surat';
    }
  }

  print("New String = ${str}");
}
