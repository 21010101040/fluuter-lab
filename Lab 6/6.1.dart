import 'dart:io';

void main(List<String> args) {
  List<dynamic> a = new List.filled(5, 0, growable: false);

  for (int i = 0; i < 5; i++) {
    stdout.write("Enter Number = ${i + 1}  ");
    a[i] = int.parse(stdin.readLineSync()!);
  }

  int f;

  for (int i = 0; i < 5; i++) {
    for (int j = i + 1; j < 5; j++) {
      if (a[i] > a[j]) {
        f = a[j];
        a[j] = a[i];
        a[i] = f;
      }
    }
  }
  print(a);
}
